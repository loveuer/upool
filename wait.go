package upool

import "runtime"

func (tp *TaskPool) Wait() {
	for {
		tp.Lock()
		rc := tp.running
		tp.Unlock()

		if rc <= 0 {
			break
		}

		runtime.Gosched()
	}
}

func (tp *TaskPool) WaitAndClose() {
	tp.Lock()
	tp.closed = true
	tp.Unlock()

	tp.cancel()

	for {
		tp.Lock()
		wc := tp.worker
		tp.Unlock()

		if wc <= 0 {
			break
		}

		runtime.Gosched()
	}
}

package upool

import (
	"fmt"
	"testing"
	"time"
)

type Task struct {
	Second int
	DoneAt time.Time
}

func (t *Task) Execute() {
	fmt.Printf("||=> Start: sleep %d seconds\n", t.Second)
	time.Sleep(time.Duration(t.Second) * time.Second)
	fmt.Printf("||=> Done:  sleep %d seconds\n", t.Second)

	t.DoneAt = time.Now()
}

func TestNormal(t *testing.T) {
	var (
		err error
	)

	pool := NewTaskPool(2, TaskOption{LogLevel: DebugLevel, IdleTime: 2})

	tasks := []*Task{{Second: 2}, {Second: 1}, {Second: 3}}

	for idx := range tasks {
		pool.Put(tasks[idx])
	}

	//pool.WaitAndClose()
	pool.Wait()

	log.Info("put 4")
	if err = pool.Put(&Task{Second: 4}); err != nil {
		t.Error("put 4 err:", err)
	}

	pool.Wait()

	if err = pool.Put(&Task{Second: 5}); err != nil {
		t.Error("put 5 err:", err)
	}

	pool.Wait()

	time.Sleep(2 * time.Second)

	pool.WaitAndClose()

	log.Info("test:task done")
}
